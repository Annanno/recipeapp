package com.example.midtermapp.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerAdapter(private val fragmentsList: MutableList<Fragment>, fragment: Fragment): FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return fragmentsList.size
    }

    override fun createFragment(position: Int): Fragment {
       return fragmentsList[position]
    }

}