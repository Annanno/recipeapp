package com.example.midtermapp.adapters.category.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.midtermapp.databinding.PopularRecipeItemBinding
import com.example.midtermapp.extensions.setImage
import com.example.midtermapp.popular.category.model.FeedRecipe

typealias ClickItem = (item: FeedRecipe) -> Unit

class PopularRecipesAdapter : RecyclerView.Adapter<PopularRecipesAdapter.ViewHolder>() {

    private var recipesList = mutableListOf<FeedRecipe>()
    var callback: ClickItem? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            PopularRecipeItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }


    override fun getItemCount() = recipesList.size

    inner class ViewHolder(private val binding: PopularRecipeItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private lateinit var model: FeedRecipe

        fun onBind() {
            model = recipesList[adapterPosition]
            binding.dishName.text = model.display.displayName
            binding.dishImage.setImage(model.display.images[0])
            binding.time.text = model.content.details.totalTime
            binding.cardView.setOnClickListener(this)

        }

        override fun onClick(p0: View?) {
            callback?.invoke(model)
        }

    }

    fun setData(list: MutableList<FeedRecipe>){
        recipesList = list
        notifyDataSetChanged()

    }


}