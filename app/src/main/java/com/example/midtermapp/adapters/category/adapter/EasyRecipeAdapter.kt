package com.example.midtermapp.adapters.category.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.midtermapp.databinding.EasyRecipeItemBinding
import com.example.midtermapp.extensions.setImage
import com.example.midtermapp.popular.category.model.FeedRecipe

typealias ClickItemE = (item: FeedRecipe) -> Unit
class EasyRecipeAdapter : RecyclerView.Adapter<EasyRecipeAdapter.ViewHolder>() {

    private var recipesList = mutableListOf<FeedRecipe>()
    var callback: ClickItemE? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            EasyRecipeItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }


    override fun getItemCount() = recipesList.size

    inner class ViewHolder(private val binding: EasyRecipeItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun onBind() {
            binding.dishName.text = recipesList[adapterPosition].display.displayName
            binding.dishImage.setImage(recipesList[adapterPosition].display.images[0])
            binding.time.text = recipesList[adapterPosition].content.details.totalTime
            binding.cardView.setOnClickListener(this)

        }

        override fun onClick(p0: View?) {
            callback?.invoke(recipesList[adapterPosition])

        }

    }

    fun setData(list: MutableList<FeedRecipe>) {
        recipesList = list
        notifyDataSetChanged()

    }

}


