package com.example.midtermapp.adapters.recipe.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.example.midtermapp.databinding.PopularRecipeDetailsItemBinding

class RecipeDetailsAdapter: RecyclerView.Adapter<RecipeDetailsAdapter.ViewHolder>() {

    private var recipeStepsList = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            PopularRecipeDetailsItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }


    override fun getItemCount() = recipeStepsList.size

    inner class ViewHolder(private val binding: PopularRecipeDetailsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind() {
            binding.step.text = "Step ${adapterPosition + 1}"
            binding.recipeTv.text = recipeStepsList[adapterPosition]

        }

    }

    fun setData(list: MutableList<String>) {
        recipeStepsList = list
        notifyDataSetChanged()

    }
}


