package com.example.midtermapp.popular.category.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class FeedRecipe(
    @Json(name = "display")
    val display: FeedRecipeItem,
    @Json(name = "content")
    val content: ContentItem,


    ): Parcelable

@JsonClass(generateAdapter = true)

@Parcelize
data class FeedRecipeItem(
    @Json(name = "displayName")
    val displayName: String,
    @Json(name = "images")
    val images: List<String>

):Parcelable

@Parcelize
data class ContentItem(
    @Json(name = "details")
    val details: DetailItem,

    @Json(name = "preparationSteps")
    val preparationSteps: List<String>?


):Parcelable

@Parcelize
data class DetailItem(
    @Json(name = "totalTime")
    val totalTime: String,

    ):Parcelable
//@JsonClass(generateAdapter = true)
//data class ImageItem(
//    @Json(name = 0.toString())
//    val im : String
//)
