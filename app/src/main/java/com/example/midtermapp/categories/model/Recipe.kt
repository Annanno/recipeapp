package com.example.midtermapp.categories.model

import com.example.midtermapp.popular.category.model.FeedRecipe
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)

data class Recipe(
    @Json(name = "feed")
    val feed: List<FeedRecipe>
)

