package com.example.midtermapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.midtermapp.categories.model.Recipe
import com.example.midtermapp.network.RetrofitInstance
import kotlinx.coroutines.launch
import retrofit2.Response

class SeasonalRecipesViewModel: ViewModel() {

    val seasonalRecipeLiveData: MutableLiveData<Response<Recipe>> = MutableLiveData()

    fun getData(){
        viewModelScope.launch {
            seasonalRecipeLiveData.value = RetrofitInstance.retrofit.getSeasonalRecipes()

        }
    }

}
