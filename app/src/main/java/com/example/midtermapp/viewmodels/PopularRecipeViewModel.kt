package com.example.midtermapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.midtermapp.network.RetrofitInstance
import com.example.midtermapp.categories.model.Recipe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class PopularRecipeViewModel: ViewModel() {

    val popularRecipeLiveData: MutableLiveData<Response<Recipe>> = MutableLiveData()

    fun getData(){
        viewModelScope.launch {

            withContext(Dispatchers.IO) {
                popularRecipeLiveData.postValue(RetrofitInstance.retrofit.getPopularRecipes())
            }


        }
    }


}