package com.example.midtermapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.midtermapp.categories.model.Recipe
import com.example.midtermapp.network.RetrofitInstance
import kotlinx.coroutines.launch
import retrofit2.Response

class CakeRecipeViewModel: ViewModel() {

    val cakeRecipeLiveData: MutableLiveData<Response<Recipe>> = MutableLiveData()

    fun getData(){
        viewModelScope.launch {
            cakeRecipeLiveData.value = RetrofitInstance.retrofit.getCakeRecipes()

        }
    }

}