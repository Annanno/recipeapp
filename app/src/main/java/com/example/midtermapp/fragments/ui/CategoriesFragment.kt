package com.example.midtermapp.fragments.ui

import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.midtermapp.adapters.ViewPagerAdapter
import com.example.midtermapp.base.BaseFragment
import com.example.midtermapp.databinding.FragmentCategoriesBinding
import com.example.midtermapp.fragments.categories.CakeRecipesFragment
import com.example.midtermapp.fragments.categories.EasyRecipesFragment
import com.example.midtermapp.fragments.categories.PopularRecipesFragment
import com.example.midtermapp.fragments.categories.SeasonalRecipesFragment
import com.google.android.material.tabs.TabLayoutMediator

class CategoriesFragment : BaseFragment<FragmentCategoriesBinding>(FragmentCategoriesBinding::inflate) {
    private lateinit var viewPager2: ViewPager2

    override fun init() {

        initViewPager()
        setTabLayout()
    }
    val fragments  = mutableListOf<Fragment>(
        PopularRecipesFragment(),
        CakeRecipesFragment(),
        EasyRecipesFragment(),
        SeasonalRecipesFragment()
    )
    private val categories = mutableListOf(
        "Popular",
        "Cookies",
        "Easy And Quick",
        "Seasonal"
    )

    private fun initViewPager(){
        val pagerAdapter = ViewPagerAdapter(fragments, this)
        viewPager2 = binding.pager
        viewPager2.adapter = pagerAdapter


    }

    private fun setTabLayout(){
        viewPager2 = binding.pager
        val tabLayout = binding.tabLayout
        TabLayoutMediator(tabLayout, viewPager2){ tab, position ->
            tab.text = categories[position]
        }.attach()
    }



//    override fun onBackPressed() {
//        if (viewPager.currentItem == 0) {
//            super.onBackPressed()
//        } else {
//            viewPager.currentItem = viewPager.currentItem - 1
//        }
//    }
}