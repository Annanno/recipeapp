package com.example.midtermapp.fragments.details


import androidx.recyclerview.widget.LinearLayoutManager
import com.example.midtermapp.adapters.recipe.adapter.RecipeDetailsAdapter
import com.example.midtermapp.base.BaseFragment
import com.example.midtermapp.databinding.FragmentPopularRecipeDetailsBinding
import com.example.midtermapp.extensions.setImage
import com.example.midtermapp.popular.category.model.FeedRecipe


class RecipeDetailsFragment : BaseFragment<FragmentPopularRecipeDetailsBinding>(FragmentPopularRecipeDetailsBinding::inflate) {

    private lateinit var popularRecipeDetailsAdapter: RecipeDetailsAdapter
    override fun init() {
        initAdapter()
        val details = arguments?.getParcelable<FeedRecipe>("dishDetails")
        binding.dishImage.setImage(details!!.display.images[0])
        details.content.preparationSteps?.let { popularRecipeDetailsAdapter.setData(it.toMutableList()) }


    }

    private fun initAdapter(){
        popularRecipeDetailsAdapter = RecipeDetailsAdapter()
        binding.recycleRecipesSteps.adapter = popularRecipeDetailsAdapter
        binding.recycleRecipesSteps.layoutManager = LinearLayoutManager(requireContext())

    }



}