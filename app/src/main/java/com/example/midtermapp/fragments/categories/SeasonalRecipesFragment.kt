package com.example.midtermapp.fragments.categories


import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.midtermapp.R
import com.example.midtermapp.adapters.category.adapter.SeasonalRecipesAdapter
import com.example.midtermapp.base.BaseFragment
import com.example.midtermapp.databinding.FragmentSeasonRecipesBinding
import com.example.midtermapp.viewmodels.SeasonalRecipesViewModel


class SeasonalRecipesFragment :
    BaseFragment<FragmentSeasonRecipesBinding>(FragmentSeasonRecipesBinding::inflate) {

    private val viewModel: SeasonalRecipesViewModel by viewModels()
    lateinit var seasonalRecipesAdapter: SeasonalRecipesAdapter

    override fun init() {
        viewModel.getData()
        initAdapter()
        observe()
    }

    private fun observe() {
        viewModel.seasonalRecipeLiveData.observe(viewLifecycleOwner, {
            binding.progressBar.isVisible = true
            seasonalRecipesAdapter.setData(it.body()?.feed!!.toMutableList())
            binding.progressBar.isVisible = false


        })
    }


    private fun initAdapter() {
        seasonalRecipesAdapter = SeasonalRecipesAdapter()
        binding.recycler.apply {
            adapter = seasonalRecipesAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        seasonalRecipesAdapter.callback = {
            val bundle = bundleOf("dishDetails" to it)

            findNavController().navigate(
                R.id.action_categoriesFragment_to_popularRecipeDetailsFragment,
                bundle
            )
        }


    }
}



