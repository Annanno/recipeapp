package com.example.midtermapp.fragments.ui


import androidx.navigation.fragment.findNavController
import com.example.midtermapp.R
import com.example.midtermapp.base.BaseFragment
import com.example.midtermapp.databinding.FragmentWelcomePageBinding


class WelcomePageFragment : BaseFragment<FragmentWelcomePageBinding>(FragmentWelcomePageBinding::inflate) {

    override fun init() {
        binding.getStartedBtn.setOnClickListener {
            findNavController().navigate(R.id.action_welcomePageFragment_to_categoriesFragment)
        }
    }

}