package com.example.midtermapp.fragments.categories


import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.midtermapp.R
import com.example.midtermapp.viewmodels.PopularRecipeViewModel
import com.example.midtermapp.adapters.category.adapter.PopularRecipesAdapter
import com.example.midtermapp.base.BaseFragment
import com.example.midtermapp.databinding.FragmentPopularRecipesBinding

class PopularRecipesFragment : BaseFragment<FragmentPopularRecipesBinding>(FragmentPopularRecipesBinding::inflate) {

    private val viewModel: PopularRecipeViewModel by viewModels()
    private lateinit var popularRecipesAdapter: PopularRecipesAdapter

    override fun init() {
        viewModel.getData()
        initAdapter()
        observe()
    }

    private fun observe() {
        viewModel.popularRecipeLiveData.observe(viewLifecycleOwner, {
           binding.progressBar.isVisible = true
                    popularRecipesAdapter.setData(it.body()?.feed!!.toMutableList())
            binding.progressBar.isVisible = false


        })


    }

    private fun initAdapter(){
        popularRecipesAdapter = PopularRecipesAdapter()
        binding.recycler.apply {
            adapter = popularRecipesAdapter
            layoutManager = LinearLayoutManager(requireContext())

        }
        popularRecipesAdapter.callback = {
            val bundle = bundleOf("dishDetails" to it)

            findNavController().navigate(R.id.action_categoriesFragment_to_popularRecipeDetailsFragment, bundle)
        }


    }
}