package com.example.midtermapp.fragments.categories

import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.midtermapp.R
import com.example.midtermapp.adapters.category.adapter.CakeRecipesAdapter
import com.example.midtermapp.base.BaseFragment
import com.example.midtermapp.databinding.FragmentCakeRecipesBinding
import com.example.midtermapp.viewmodels.CakeRecipeViewModel
import kotlinx.coroutines.launch

class CakeRecipesFragment : BaseFragment<FragmentCakeRecipesBinding>(FragmentCakeRecipesBinding::inflate) {
    private val viewModel: CakeRecipeViewModel by viewModels()
    private lateinit var cakeRecipesAdapter: CakeRecipesAdapter

    override fun init() {
        viewModel.getData()
        initAdapter()
        observe()
    }

    private fun observe() {
        viewModel.cakeRecipeLiveData.observe(viewLifecycleOwner, {
            binding.progressBar.isVisible = true
            cakeRecipesAdapter.setData(it.body()?.feed!!.toMutableList())
            binding.progressBar.isVisible = false


        })

    }

    private fun initAdapter(){
        cakeRecipesAdapter = CakeRecipesAdapter()
        binding.recycler.apply {
            adapter = cakeRecipesAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        cakeRecipesAdapter.callback = {
            val bundle = bundleOf("dishDetails" to it)

            findNavController().navigate(R.id.action_categoriesFragment_to_popularRecipeDetailsFragment, bundle)
        }


    }

}