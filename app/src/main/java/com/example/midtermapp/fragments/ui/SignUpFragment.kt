package com.example.midtermapp.fragments.ui


import android.util.Patterns
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.midtermapp.base.MainActivity
import com.example.midtermapp.R
import com.example.midtermapp.base.BaseFragment
import com.example.midtermapp.databinding.FragmentSignUpBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class SignUpFragment : BaseFragment<FragmentSignUpBinding>(FragmentSignUpBinding::inflate) {

    private lateinit var auth: FirebaseAuth

    override fun init() {
        auth = Firebase.auth

        binding.signIn.setOnClickListener {
            it.findNavController().navigate(R.id.action_signUpFragment_to_logInFragment)
        }

        binding.signUp.setOnClickListener {
            signUpUser()

        }
    }

    private fun signUpUser() {
        val email = binding.email.text.toString()
        val password = binding.password.text.toString()
        val repeatPassword = binding.repeatPassword.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email)
                .matches() && password == repeatPassword
        ) {
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(MainActivity()) { task ->
                    if (task.isSuccessful) {
                        val user = auth.currentUser
                        updateUI(user)
                        findNavController().navigate(R.id.action_signUpFragment_to_welcomePageFragment)
                        Toast.makeText(context, "User added successfully", Toast.LENGTH_SHORT)
                            .show()

                    } else {
                        Toast.makeText(
                            context, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                        updateUI(null)
                    }
                }
        } else Toast.makeText(context, "Fill all fields", Toast.LENGTH_SHORT).show()
    }

    private fun updateUI(user: FirebaseUser?) {

    }
}




