package com.example.midtermapp.network

import com.example.midtermapp.categories.model.Recipe
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RecipeService {

    @GET("feeds/list?limit=18&start=0&tag=list.recipe.popular")
    suspend fun getPopularRecipes(@Query("limit") limit: Int = 18, @Query("start") start: Int = 0): Response<Recipe>

    @GET("feeds/list?limit=18&start=0&tag=list.recipe.search_based%3Afq%3Aattribute_s_mv%3A(dish%5C%5Edish%5C-cookie)")
    suspend fun getCakeRecipes(@Query("limit") limit: Int = 18, @Query("start") start: Int = 0): Response<Recipe>

    @GET("feeds/list?limit=18&start=0&tag=list.recipe.search_based%3Asearch%3Aexp_sqe")
    suspend fun getEasyRecipes(@Query("limit") limit: Int = 18, @Query("start") start: Int = 0): Response<Recipe>

    @GET("feeds/list?limit=18&start=0&tag=list.recipe.search_based%3Adisplay%3ASeasonal%2Csearch%3Aexp_sseas")
    suspend fun getSeasonalRecipes(@Query("limit") limit: Int = 18, @Query("start") start: Int = 0): Response<Recipe>


}
