package com.example.midtermapp.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitInstance {

    private val client = OkHttpClient().newBuilder().addInterceptor { chain ->
        val original = chain.request().newBuilder()
        val requestBuilder = original.header("x-rapidapi-host", "yummly2.p.rapidapi.com")
            .header("x-rapidapi-key", "5dcb66c6bcmsh9074888444d4843p1d4e5fjsnc2fd92f8a23a")

        val request = requestBuilder.build()
        chain.proceed(request)

    }



    val retrofit: RecipeService by lazy {
        Retrofit.Builder()
            .baseUrl("https://yummly2.p.rapidapi.com")
            .client(client.build())
            .addConverterFactory(MoshiConverterFactory.create(
                Moshi.Builder()
                    .addLast(KotlinJsonAdapterFactory())
                    .build()
            )
            ).build().create(RecipeService::class.java)
    }


}