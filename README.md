# RECIPE APP #

Simple recipe app to search recipes according categories


### Usage ###

The user has to register but if already has an account only needs to be logged in. All you need is an email and password.
Then, the user is able to see the steps of preparing the desired dish according to the given categories.

### API ###

App uses Yummly API to get data about recipes

api: https://rapidapi.com/apidojo/api/yummly2/

### Used resources ###

* Retrofit
* ViewPager2
* Mvvm
* Material components
* Navigation
* Firebase authentication

